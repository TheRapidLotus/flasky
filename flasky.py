from flask import Flask, jsonify
import os

flasky = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))


# home endpoint
@flasky.route('/')
def home():
    return jsonify(data='Welcome to the PAGE!')


if __name__ == '__main__':
    flasky.run(host='0.0.0.0', port=5000)
